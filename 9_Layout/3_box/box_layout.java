import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

public class box_layout extends JFrame{

	JButton button1, button2, button3, button4, button5;
	
	public static void main(String[] args)
	{
		
		new box_layout();
		
	}
	
	public box_layout(){
		
		// Create the frame, position it and handle closing it
		
		this.setSize(400,400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("My Sixth Frame");
		
		//Buttons
		button1 = new JButton("Button 1");
		button2 = new JButton("Button 2");
		button3 = new JButton("Button 3");
		button4 = new JButton("Button 4");
		button5 = new JButton("Button 5");

		/* BOX LAYOUT */
		
		Box theBox = Box.createHorizontalBox(); 	// You can also use Box theBox = Box.createVerticalBox();

		/*  Normal postion 

		theBox.add(button1);
		theBox.add(button2);
		theBox.add(button3);
		theBox.add(button4);
		*/
		
		// You can also separate the components with struts
		
		theBox.add(button1);
		theBox.add(Box.createHorizontalStrut(10)); //Horizontal space b/w but1 to but2 is 10
		theBox.add(button2);
		theBox.add(Box.createHorizontalStrut(20));
		theBox.add(button3);
		theBox.add(Box.createHorizontalStrut(2));
		theBox.add(button4);

		
		/*Some other property

		// A rigid area gives you the option to space using
		// horizontal and vertical spacing
		
		theBox.add(button1);
		theBox.add(button2);
		theBox.add(Box.createRigidArea(new Dimension(30, 20)));
		
		// When you use a glue you position the components as
		// far apart as possible while remaining on the screen
		// There is also a createVerticalGlue
		
		theBox.add(Box.createHorizontalGlue());
		theBox.add(button3);
		*/
		
		this.add(theBox);	// this.add(thePanel);  Don't use for BOX LAYOUT
		
		this.setVisible(true);
	}
	
}
