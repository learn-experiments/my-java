import javax.swing.*;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;


public class flow_layout extends JFrame
{

	JButton button1, button2, button3, button4, button5;

	
	public static void main(String[] args){
		
		new flow_layout();
		
	}
	
	public flow_layout()
	{
		
		// Create the frame, position it and handle closing it
		
		this.setSize(400,400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("My Flow Layout Frame");
		 
		/* FLOW LAYOUT*/
		// Create a flow layout (Default)
		JPanel thePanel = new JPanel();
		
		// Define the flow layout alignment
		// FlowLayout.RIGHT, FlowLayout.CENTER
		
		thePanel.setLayout(new FlowLayout(FlowLayout.LEFT,70,20));  //LEFT -> side , 70 -> margin x,20 ->margin y
		
		// You can also define the pixels that separate the components
		// FlowLayout(alignment, horz gap, vertical gap)
		  
		 button1 = new JButton("Button 1");
		 button2 = new JButton("Button 2");
		 thePanel.add(button1);
		 thePanel.add(button2);
		 
		 /*END FLOW LAYOUT*/
		
		this.add(thePanel);
		
		this.setVisible(true);
	}
	
}
