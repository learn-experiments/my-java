import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Dimension;


public class border_layout extends JFrame
{

	JButton button1, button2, button3, button4, button5;

	
	public static void main(String[] args)
	{
		
		new border_layout();
		
	}
	
	public border_layout()
	{
		
		// Create the frame, position it and handle closing it
		
		this.setSize(400,400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("My Border Layout Frame");
		 
		/* BORDER LAYOUT */
		
		JPanel thePanel = new JPanel();
		
		thePanel.setLayout(new BorderLayout());
		
		// Create buttons
		
		button1 = new JButton("Button 1");
		button2 = new JButton("Button 2");
		button3 = new JButton("Button 3");
		button4 = new JButton("Button 4");
		button5 = new JButton("Button 5");
		
		// If you put components in the same space the
		// last one in stays and everything else goes
		// EX.
		// thePanel.add(button1, BorderLayout.NORTH);
		// thePanel.add(button2, BorderLayout.NORTH);
		// Only button2 shows
		
		
		thePanel.add(button1, BorderLayout.NORTH);
		thePanel.add(button2, BorderLayout.SOUTH);
		thePanel.add(button3, BorderLayout.EAST);
		thePanel.add(button4, BorderLayout.WEST);
		thePanel.add(button5, BorderLayout.CENTER);
		
		
		/* If you want more than one component to show
		// up in the same part of a border layout put
		// them in a panel and then add the panel to
		// the border layout panel
		*/
		
		JPanel thePanel2 = new JPanel();
		JButton but1,but2;
		but1 = new JButton("pair 1");
		but2 = new JButton("pair 2");
		
		thePanel2.add(but1);
		thePanel2.add(but2);
		
		thePanel.add(thePanel2, BorderLayout.SOUTH);
				
		this.add(thePanel);
		
		this.setVisible(true);
	}
	
}
