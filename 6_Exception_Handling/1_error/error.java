/*
Exception Handling
	Types of Exception
		1.java.lang.RunTimeException
		2.java.lang.Exception
	
	Common Exception
		1.ArithmeticException
		2.ClassNotFoundException
		3.IllegalArgumentException
		4.IndexOutOfBoundsException
		5.InputMismatchException
		6.IOException
		
	Keywords
		1.try     => monitoring the exception
		2.catch   =>handle the particular exception
		3.throw   => throw specific exception
		4.throws  => throw specifies exceptions by a particular method
		5.finally => even exception may or not occur,this block always execute
*/

public class error
{
	//main function
	public static void main(String[] args)
	{
		divideByZero(5);
	}
	public static void divideByZero(int a)
	{
		try
		{
			System.out.println(a/0);
		}
		catch(Exception e)
		{
			//custom message
			System.out.println("Divide by Zero is not possible");
			
			//error message only
			System.out.println(e.getMessage());
			
			//error type and error
			System.out.println(e);
			System.out.println(e.toString());
			
			//print some error info
			e.printStackTrace();
		}
	}
}
