import java.lang.Exception;

//extend the own class to Exception
class MyOwnExcEx extends Exception
{
	MyOwnExcEx(String msg)
	{
		super(msg);
	}
public static void main(String[] args)
{
		int age=5;
				
		//user defined Exception
		try
		{
			if(age<18) 
				//define new throw exception
				throw new MyOwnExcEx("You are not 18+ ");
		}
		catch(MyOwnExcEx e)
		{
			System.out.println(e.getMessage());
		}
	
}
}