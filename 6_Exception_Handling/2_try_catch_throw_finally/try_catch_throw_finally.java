/*
Exception Handling
	Types of Exception
		1.java.lang.RunTimeException
		2.java.lang.Exception
	
	Common Exception
		1.ArithmeticException
		2.ClassNotFoundException
		3.IllegalArgumentException
		4.IndexOutOfBoundsException
		5.InputMismatchException
		6.IOException
		
	Keywords
		1.try     => monitoring the exception
		2.catch   =>handle the particular exception
		3.throw   => throw specific exception
		4.throws  => throw specifies exceptions by a particular method
		5.finally => even exception may or not occur,this block always execute
*/

public class try_catch_throw_finally
{
	
//main function
public static void main(String[] args)
{
		int a=5,b=0;
		
		//call function
		divideByZero(a,b);
}
public static void divideByZero(int a,int b) throws ArithmeticException,ArrayIndexOutOfBoundsException
{
	int c=-1;
	try
	{
		//this line to occur ArithmeticException
		c=a/b;
		
		//this line to occur ArrayIndexOutOfBoundsException
		int d[]={1,3};
		int f=d[3];
		
	}
	catch(ArithmeticException e)
	{
		//Display error message
		System.out.println("Divide by Zero is not possible");
				
	}
	catch(ArrayIndexOutOfBoundsException e)
	{
		//Display error message
		System.out.println("Input Output Exception");
	}
	finally
	{
		if(c==-1)
			System.out.println("Error Occurred");
		else
			System.out.println("Error not Occurred");
	}
}
}