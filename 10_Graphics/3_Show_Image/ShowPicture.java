import javax.swing.JFrame; 
import javax.swing.ImageIcon; 
import javax.swing.JLabel; 
import java.awt.Container;
class ShowPicture 
{       
 public static void main(String args[]) 
 {        
 JFrame frame = new JFrame();        
 ImageIcon icon = new ImageIcon("1.jpg");        
 JLabel label = new JLabel(icon);                
 frame.add(label);        
 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
 frame.pack();        
 frame.setVisible(true);    
 }
}