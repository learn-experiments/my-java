//Step 1:-Import the net package which supports networking features in Java. 
//Step 2:-Write the input data into an object from the keyboard through BufferedStream class. 
//Step 3:-Create a new object for Socket s and Write the input data into another object called dos using writeBytes() method. 
//Step 4:-Read the copied string into br using readLine() method and close the file. 
//Step 5:-On executing the program,the server sends a message to the client. 
//Step 6:-For the server side program, create an object for ServerSocket class  and write input data into it. 
//Step 7:-Write the data into the dataoutputstream object dos. 
//Step 8:-close the object and socket.
// Client Program:-
import java.net.*;
import java.io.*; 
class Client 
{   
 public static void main(String args[])throws IOException    
 {       
 Socket soc=null;      
 String str=null;
 BufferedReader br=null;  
 DataOutputStream dos=null; 
 BufferedReader kyrd=new BufferedReader(new InputStreamReader(System.in));   
 try      
 {  
 soc=new Socket(InetAddress.getLocalHost(),95); 
 br=new BufferedReader(new InputStreamReader(soc.getInputStream())); 
 dos=new DataOutputStream(soc.getOutputStream());      
 }        
 catch(UnknownHostException uhe) 
 {
	 System.out.println("Unknown Host");            
	 System.exit(0);        
 }        
 System.out.println("To start the dialogue type the message in this client window \n Type exit to end");
 boolean more=true;       
 while(more)       
 {           
 str=kyrd.readLine();            
 dos.writeBytes(str);   
 dos.write(13);   
 dos.write(10);  
 dos.flush();  
 String s,s1;   
 s=br.readLine();     
 System.out.println("From server :"+s);   
 if(s.equals("exit"))
	 break;        
 }       
 br.close();     
 dos.close();     
 soc.close();   
 } 
 }
