import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.*;
import java.awt.event.*;

public class swing_event_listener extends JFrame
{
	int buttonClciked;
	JButton button1;
	JTextArea txtArea1;
	public static void main(String[] args)
	{
	//define new swing class with JFreme
	new swing_event_listener();
	}

	//call swing class
	public swing_event_listener()
	{
	//set size of Frame
	this.setSize(400,400);

	//define Toolkit and find screen size
	Toolkit tk = Toolkit.getDefaultToolkit();
	Dimension dim = tk.getScreenSize();
	
	int xPos = (dim.width / 2) - (this.getWidth() / 2);
	int yPos = (dim.height / 2) - (this.getHeight() / 2);
	this.setLocation(xPos, yPos);
	
	//Set Title 
	this.setTitle("My First Event Hanling Frame");

	//create Panel
	JPanel thePanel = new JPanel();
		
	//Button
	button1 = new JButton("Click here");
	//button1.setText("My Button");

	
	ListenForButton lbut1 = new ListenForButton();
	button1.addActionListener(lbut1);
	thePanel.add(button1);
		
	//TextArea
	txtArea1 = new JTextArea(15,30);  //txtArea1 with 15 height and 30 weight
	txtArea1.setText("Where Display to How many times click the above button");
	txtArea1.setLineWrap(true);  //wrap the content 
	txtArea1.setWrapStyleWord(true);

	//Scroll bar
	JScrollPane scrollpane1 = new JScrollPane(txtArea1, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	thePanel.add(scrollpane1);
	
	//View Panel
	this.add(thePanel);
	
	//Focus to Particular field
	txtArea1.requestFocus();
	
	//Close operation
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	//Visibility true
	this.setVisible(true);
	
	}

	//implement ListenForButton
	private class ListenForButton implements ActionListener
	{
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == button1)
		{
			buttonClciked++;
			txtArea1.append("\nButton clicked " +buttonClciked+ " times");
			//e.getSource().toString()
		}
	}
	}

}
