import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.*;


public class swing_basic extends JFrame
{

public static void main(String[] args)
{
//define new swing class with JFreme
new swing_basic();
}

//call swing class
public swing_basic()
{
	//set size of Frame
	this.setSize(400,500);

	//position of Frame relative=null to centre position
	this.setLocationRelativeTo(null);
	
	/*
	Set Particular location
	int x=200;
	int y=200;
	//Location
	this.setLocation(x,y);
	*/
	
	//Resize option
	this.setResizable(false);
	
	//Set Title 
	this.setTitle("My First Frame");
	
	//Define Panel 
	JPanel thePanel = new JPanel();
	
	//Define Label
	JLabel label1 = new JLabel("This is My 1st Label");
	
	//Add Panel with Label
	thePanel.add(label1);
	
	
	//Label2 define,set text and add to panel
	JLabel label2 = new JLabel();
	label2.setText("\nThis is My 2nd Label");
	thePanel.add(label2);
	
	//set tool tip of the label
	label2.setToolTipText("This is Tool tip of Label2");
	
	//JButton
	JButton button1 = new JButton();
	button1.setText("My Button");
	//button1.setBorderPainted(false);
	//button1.setContentAreaFilled(false);
	button1.setToolTipText("This is my button1");
	thePanel.add(button1);
	
	//TextField
	JTextField txtbox1 = new JTextField("Type here",15);
	txtbox1.setColumns(10);
	txtbox1.setText("Type again");
	thePanel.add(txtbox1);
	
	//TextArea
	JTextArea txtArea1 = new JTextArea(10,30);  //txtArea1 with 15 height and 30 weight
	txtArea1.setText("This is whole bunch of that is important");
	//txtArea1.setLineWrap(true);  //wrap the content 
	JScrollPane scrollpane1 = new JScrollPane(txtArea1, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	thePanel.add(scrollpane1);

	// Defines the font to use in the text field
	Font font = new Font("Helvetica", Font.PLAIN, 14);
	txtArea1.setFont(font);
		
	
	//Checkbox
	JCheckBox chbox1 = new JCheckBox("Tamil");
	JCheckBox chbox2 = new JCheckBox("English");
	thePanel.add(chbox1);
	thePanel.add(chbox2);
	chbox1.setSelected(true);
	
	//RadioButton
	JRadioButton radbut1 = new JRadioButton("Male");
	JRadioButton radbut2 = new JRadioButton("FeMale");
	thePanel.add(radbut1);
	thePanel.add(radbut2);
	radbut1.setSelected(true);
	
	//Slider
	JSlider slider1 = new JSlider(0, 10, 1); //JSlider(start,end,increase or decrease)
	slider1.setMinorTickSpacing(1);
	slider1.setMajorTickSpacing(1);//min to major distance b/w each space
	slider1.setPaintTicks(true);
	slider1.setPaintLabels(true);
	thePanel.add(slider1);
	
	//ComboBox
	JComboBox cmbox1 = new JComboBox();
	cmbox1.addItem("CSE");
	cmbox1.addItem("IT");
	cmbox1.addItem("ECE");
	cmbox1.addItem("MECH");
	thePanel.add(cmbox1);
	
	//List
	String[] books = {"CA","PDS","OS","DSP"};
	JList list1 = new JList(books);
	list1.setFixedCellHeight(30);
	list1.setFixedCellWidth(150);
	thePanel.add(list1);
	
	//Spinner
	JSpinner spin1 = new JSpinner(new SpinnerNumberModel(4,2,10,2));	//SpinnerNumberModel(start,min,max,increase or decrease)
	JSpinner spin2 = new JSpinner(new SpinnerDateModel());
	thePanel.add(spin1);
	thePanel.add(spin2);
	
	//View Panel
	this.add(thePanel);
	
	//Focus to Particular field
	txtArea1.requestFocus();
	
	//Close operation
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	//Visibility true
	this.setVisible(true);
}
}
