//import scanner header file
import java.util.Scanner;
public class scanner
{
	//main function
	public static void main(String[] args)
	{
		//declare new scanner method variable
		Scanner input_value = new Scanner(System.in);
		int i=1;
		int value[]=new int[10];
		
		//get five value continues
		System.out.println("Enter the Values :");
		while(i<=5)
		{
			value[i] = input_value.nextInt();
			i++;
		}
		i=0;
		while(i<=5)
		{
			System.out.println("The Value["+i+"] is :" +value[i]);
			i++;
		}
		
		//check the value is integer or not
		System.out.println("Enter the Value to check :");
		if(input_value.hasNextInt())
		{
			int val= input_value.nextInt();
			System.out.println("The Value "+val+" is Integer");
		}
		else
		{
			System.out.println("The Value is not Integer");
		}
	}
}
