
//main class file name
public class Basic
{

//main function of the program
public static void main(String[] args)
{

//Variables Data type with large value
byte a=127;
short b=32767;
int c=2100000000;
long d=9220000000000000000L;
float e=3.14F;
double f=3.14823794872734872347923748D;
boolean g=true;

//characters
char h=65;
char i='A';
String name = "Mani";

//Display this Values use Default Function
System.out.println("Display Data types and Their Max Value");
System.out.println("Flaot" +Float.MAX_VALUE);
System.out.println("Integer" +Integer.MAX_VALUE);
System.out.println("Long" +Long.MAX_VALUE);
System.out.println("Double" +Double.MAX_VALUE);
System.out.println("Byte" +Byte.MAX_VALUE);

//Print Function to Display Values
System.out.println('\n');
System.out.println("Print the Values \n Integer \t" + c + "\n Float \t" + e);
System.out.println("Print the Char values \n ASCII  \t" + h + "\n Single \t" + i);

//Any type To String
System.out.println("\nThe Integer Value "+ c + " to String "+ Integer.toString(c));
System.out.println("The Byte Value "+ a + " to String "+ Byte.toString(a));

//Double to Integer
//float double char not work
System.out.println("\nThe Double Value "+ f +" to Integer "+ (int)f);

//String to Integer
//also Available parseShort,parseLong,parseByte,parseFloat,parseDouble,parseboolean 
String ItoS = Integer.toString(c);
System.out.println("\nThe String Value to Integer \nalready Integer to String Values only change "+ Integer.parseInt(ItoS));


} 
}
