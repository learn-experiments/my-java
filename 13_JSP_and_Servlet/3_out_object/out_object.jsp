<!-- JSP = HTML + JAVA -->

<html>
<body>

<!-- JSP Scriptlet -->
<%
System.out.println("\nHello...!"); //This line not display only view in log file
java.util.Date date_val= new java.util.Date(); 
%>
Time is <%= date_val%>
</br>

<!-- JSP Out Object -->
<%
out.println("<br>");
out.println("\nHello... This is My Out Object Content!"); //This line display in Browser
out.println("<br>");
out.println("\n Time Now :"+date_val);
%>
</body>
</html>