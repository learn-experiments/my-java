JSP:
=> use dynamic web pages
=> easy to develop
=> directly open in browser
=> use <% ..... %> with in java code also html code in outer
=> no compile directly use
=> security less
=> not suitable high end works


Servlet (java):
=> use dynamic web pages
=> much better to develop
=> fully code in java
=> not allowed to directly open in browser
=> compile and class file to use
=> security high
=> suitable high end works

1.Folders Creation

	a. open tomcat/webapps folder
	b. then create new folder any name like : my_servlet
	c. inside my_servlet => WEB-INF and html file
	d. then inside WEB-INF => src , classes , lib , web.xml files
	
	where
	
	my_servlet => project folder
	WEB-INF => use in servlet projects bcz of security purpose
	src => source java file .java file
	classes => class file of src java file
	lib => library java,jar files in that folder
	web.xml => use project info and flow of control the servlet and html
	
2. In web.xml file

	<servlet>
		<servlet_name>2nd_b</servlet_name>
		<servlet_class>3rd</servlet_class>
	</servlet>
	<servlet_mapping>
		<servlet_name>2nd_a</servlet_name>
		<url_pattern>/1st</url_pattern>
	</servlet_mapping>
	
	works:
		
		a. now servlet not directly view in browser so, web.xml to map it
		b. localhost:8080/my_servlet/1st => 1st is the url_pattern check the /1st flow
		c. 1st url_pattern check their servlet_name 
		d. then this servlet_name check with their servlet_class to call this servlet on browser
	
3.Compile java file

	syntax => javac -cp <tomat_lib_servlet_api.jar file> javafile
	ex => javac -cp 
	
	where, cp => classpath

4.CLASS_PATH

	a. use to servlet programs to compile (bcz Servlet methods don't have in Java)
	b. goto "environmental variable -> System Variable -> New"
	c. Variable name : CLASSPATH
	d. Variable value : tomat_lib_servlet_api.jar file

	
Note:
	a. if any changes in any file to reload the project in tomcat manager