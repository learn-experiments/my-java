public class math
{
	//main function
	public static void main(String[] args)
	{
		//random number between 100
		int randnum = (int)(Math.random() * 100);
		System.out.println("Random Value is "+randnum);
		
		//some math function
		System.out.println("Maximum Value is " +Math.max(5,3));
		System.out.println("Minimum Value is " +Math.min(5,3));
		double a=5.3;
		System.out.println("The Value a="+a+"\nRounded Value is " +Math.round(a)+"\nFloor Value is "+Math.floor(a)+"\nCeil Value is "+Math.ceil(a));
	}
}
